<?php
	session_start();
	include 'includes/conn.php';

	if(isset($_POST['login'])){
		$username = $_POST['username'];
		$password = $_POST['password'];

		$sql = "SELECT * FROM superadmin WHERE username = '$username'";
		$query = $conn->query($sql);

		if($query->num_rows < 1){
			$_SESSION['error'] = '<div class="text-danger">Username does not exist!</div>';
		}
		else{
			$row = $query->fetch_assoc();
			if(password_verify($password, $row['password'])){
				$_SESSION['superadmin'] = $row['id'];
			}
			else{
				$_SESSION['error'] = '<div class="text-danger">Password is Incorrect!</div>';
			}
		}
		
	}
	else{
		$_SESSION['error'] = 'Input Admin Credentials First';
	}

	header('location: index.php');

?>