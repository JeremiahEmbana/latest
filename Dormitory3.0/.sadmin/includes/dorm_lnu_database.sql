

CREATE TABLE `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `password` varchar(60) NOT NULL,
  `firstname` varchar(30) NOT NULL,
  `lastname` varchar(30) NOT NULL,
  `address` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `contact` varchar(100) NOT NULL,
  `photo` varchar(200) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

INSERT INTO admin VALUES("1","username","$2y$10$UmEi94KQFWXwypsWNdot1e0o/x46SY71Pdnx2OeQeS8vHpqlxxUZm","Admin","Admin","Tacloban City","admin@gmail.com","09063774018","profile.jpg","2021-06-03 00:00:00");



CREATE TABLE `borrow` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(15) NOT NULL,
  `equipment_id` int(11) NOT NULL,
  `date_borrow` timestamp NOT NULL DEFAULT current_timestamp(),
  `status` int(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `student_id` (`student_id`),
  KEY `equipment_id` (`equipment_id`),
  CONSTRAINT `borrow_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `students` (`student_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `borrow_ibfk_2` FOREIGN KEY (`equipment_id`) REFERENCES `equipments` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=157 DEFAULT CHARSET=latin1;




CREATE TABLE `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

INSERT INTO category VALUES("1","Appliances");
INSERT INTO category VALUES("2","Hardware Supply");
INSERT INTO category VALUES("4","Furniture");
INSERT INTO category VALUES("5","Bed Equipment");



CREATE TABLE `checkin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transient_id` varchar(100) NOT NULL,
  `room_id` int(15) NOT NULL,
  `date_in` date NOT NULL,
  `time_in` time NOT NULL,
  `date_out` date NOT NULL,
  `time_out` time NOT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `transient_id` (`transient_id`),
  KEY `room_id` (`room_id`),
  CONSTRAINT `checkin_ibfk_1` FOREIGN KEY (`transient_id`) REFERENCES `transient` (`transient_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `checkin_ibfk_2` FOREIGN KEY (`room_id`) REFERENCES `rooms` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

INSERT INTO checkin VALUES("26","UON1059","14","2021-07-29","09:33:00","2021-07-31","09:33:00","0");



CREATE TABLE `checkout` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transient_id` varchar(100) NOT NULL,
  `room_id` int(15) NOT NULL,
  `date_in` date NOT NULL,
  `time_in` time NOT NULL,
  `date_out` date NOT NULL,
  `time_out` time NOT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `transient_id` (`transient_id`),
  KEY `room_id` (`room_id`),
  CONSTRAINT `checkout_ibfk_1` FOREIGN KEY (`transient_id`) REFERENCES `transient` (`transient_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `checkout_ibfk_2` FOREIGN KEY (`room_id`) REFERENCES `rooms` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

INSERT INTO checkout VALUES("16","UON1059","2","2021-06-29","09:32:00","2021-06-30","09:32:00","0");



CREATE TABLE `course` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text NOT NULL,
  `code` varchar(15) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

INSERT INTO course VALUES("1","Bachelor of Science in Business Administration","BSBA");
INSERT INTO course VALUES("2","Bachelor of Science in Information Technology","BSIT");
INSERT INTO course VALUES("3","Bachelor of Science in Hospitality Management","BSHM");



CREATE TABLE `equipments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(20) NOT NULL,
  `category_id` int(11) NOT NULL,
  `title` text NOT NULL,
  `quantity` bigint(255) NOT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`),
  CONSTRAINT `equipments_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

INSERT INTO equipments VALUES("20","S06","5","Mattress/Foam","0","0");
INSERT INTO equipments VALUES("21","P28","5","Pillow","14","0");
INSERT INTO equipments VALUES("22","K56","5","Blanket","15","0");
INSERT INTO equipments VALUES("23","D58","5","Bed Sheet","35","0");
INSERT INTO equipments VALUES("24","N34","1","Electric Fan","11","0");
INSERT INTO equipments VALUES("25","C94","2","Clothes Drawer","7","0");



CREATE TABLE `event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event_category_id` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `location` varchar(100) NOT NULL,
  `date` date NOT NULL,
  `time_start` time NOT NULL,
  `time_end` time NOT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `event_category_id` (`event_category_id`),
  CONSTRAINT `event_ibfk_1` FOREIGN KEY (`event_category_id`) REFERENCES `event_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

INSERT INTO event VALUES("14","2","Conucting clean up drive","LNU Dorm Grounds","2021-06-30","09:30:00","10:30:00","1");



CREATE TABLE `event_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

INSERT INTO event_category VALUES("1","Meeting");
INSERT INTO event_category VALUES("2","Clean Up Drive");
INSERT INTO event_category VALUES("4","Others");



CREATE TABLE `logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;




CREATE TABLE `paid` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(15) NOT NULL,
  `date_from` date NOT NULL,
  `date_to` date NOT NULL,
  `date_paid` timestamp NOT NULL DEFAULT current_timestamp(),
  `status` int(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `student_id` (`student_id`),
  CONSTRAINT `paid_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `students` (`student_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=122 DEFAULT CHARSET=latin1;

INSERT INTO paid VALUES("120","1800567","2021-06-29","2021-07-29","2021-06-29 09:27:50","1");
INSERT INTO paid VALUES("121","1800649","2021-06-29","2021-07-29","2021-06-29 09:29:42","1");



CREATE TABLE `password_reset` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `token` varchar(100) NOT NULL,
  `expDate` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;




CREATE TABLE `pending` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(15) NOT NULL,
  `equipment_id` int(11) NOT NULL,
  `note` varchar(255) NOT NULL,
  `feedback` varchar(255) NOT NULL,
  `date_pending` timestamp NOT NULL DEFAULT current_timestamp(),
  `status` int(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `student_id` (`student_id`),
  KEY `equipment_id` (`equipment_id`),
  CONSTRAINT `pending_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `students` (`student_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pending_ibfk_2` FOREIGN KEY (`equipment_id`) REFERENCES `equipments` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=latin1;

INSERT INTO pending VALUES("69","1800649","21","dscdsdc","","2021-06-29 12:37:12","0");
INSERT INTO pending VALUES("70","1800649","21","borrow","","2021-06-29 23:47:37","0");



CREATE TABLE `promissory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NOT NULL,
  `date_from` date NOT NULL,
  `date_to` date NOT NULL,
  `pnote` varchar(255) NOT NULL,
  `date_promissory` timestamp NOT NULL DEFAULT current_timestamp(),
  `deadline` date NOT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `student_id` (`student_id`),
  CONSTRAINT `promissory_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `students` (`student_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=latin1;

INSERT INTO promissory VALUES("42","1800649","2021-06-29","2021-07-29","i will pay by end of the month","2021-06-29 09:29:14","2021-06-30","1");



CREATE TABLE `returns` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(15) NOT NULL,
  `equipment_id` int(11) NOT NULL,
  `date_return` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `student_id` (`student_id`),
  KEY `equipment_id` (`equipment_id`),
  CONSTRAINT `returns_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `students` (`student_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `returns_ibfk_2` FOREIGN KEY (`equipment_id`) REFERENCES `equipments` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=75 DEFAULT CHARSET=latin1;

INSERT INTO returns VALUES("74","1800649","21","2021-06-29 09:25:32");



CREATE TABLE `rooms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `room` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

INSERT INTO rooms VALUES("1","1-1","1st Floor - Room 1");
INSERT INTO rooms VALUES("2","1-2","1st Floor - Room 2");
INSERT INTO rooms VALUES("3","1-3","1st Floor - Room 3");
INSERT INTO rooms VALUES("4","1-4","1st Floor - Room 4");
INSERT INTO rooms VALUES("5","1-5","1st Floor - Room 5");
INSERT INTO rooms VALUES("6","2-1","2nd Floor - Room 1");
INSERT INTO rooms VALUES("7","2-2","2nd Floor - Room 2");
INSERT INTO rooms VALUES("8","2-3","2nd Floor - Room 3");
INSERT INTO rooms VALUES("9","2-4","2nd Floor - Room 4");
INSERT INTO rooms VALUES("10","2-5","2nd Floor - Room 5");
INSERT INTO rooms VALUES("11","3-1","3rd Floor - Room 1");
INSERT INTO rooms VALUES("12","3-2","3rd Floor - Room 2");
INSERT INTO rooms VALUES("13","3-3","3rd Floor - Room 3");
INSERT INTO rooms VALUES("14","3-4","3rd Floor - Room 4");
INSERT INTO rooms VALUES("15","3-5","3rd Floor - Room 5");
INSERT INTO rooms VALUES("16","4-1","4th Floor - Room 1");
INSERT INTO rooms VALUES("17","4-2","4th Floor - Room 2");
INSERT INTO rooms VALUES("18","4-3","4th Floor - Room 3");
INSERT INTO rooms VALUES("19","4-4","4th Floor - Room 4");
INSERT INTO rooms VALUES("20","4-5","4th Floor - Room 5");



CREATE TABLE `stud_timeout` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(15) NOT NULL,
  `reason` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

INSERT INTO stud_timeout VALUES("10","1800649","Going home","2021-06-29 23:46:57");



CREATE TABLE `students` (
  `student_id` int(15) NOT NULL,
  `password` varchar(255) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `gender` varchar(100) NOT NULL,
  `address` varchar(255) NOT NULL,
  `contact` varchar(20) NOT NULL,
  `email` varchar(255) NOT NULL,
  `room_id` int(11) NOT NULL,
  `privilege` varchar(255) NOT NULL,
  `guardian` varchar(50) NOT NULL,
  `guardian_contact` varchar(20) NOT NULL,
  `photo` varchar(200) NOT NULL,
  `course_id` int(11) NOT NULL,
  `status` int(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`student_id`),
  KEY `room_id` (`room_id`),
  KEY `course_id` (`course_id`),
  CONSTRAINT `students_ibfk_1` FOREIGN KEY (`room_id`) REFERENCES `rooms` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `students_ibfk_2` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO students VALUES("1800234","$2y$10$ritHvvjd5C1MamkadCcs9OsnhV.4g9RGXQhTOzboJmNyDOrUQ9xCu","Lynette","Pomasin","Female","Samar","+639876543214","pomasin@gmail.com","18","Non-Athlete","Mrs. Pomasin","09123232233","","2","0","2021-06-25 00:37:31");
INSERT INTO students VALUES("1800435","$2y$10$hSTVMJe9M4ZSnP6fSSfkTu8Z077d82NVBjY.0Z1u7VaOMIJt5cHDK","Jalyne","Terrora","Female","Jaro, Leyte","+639534545345","terrora@gmail.com","10","Athlete","Mr. Terrora","42323425","","2","0","2021-06-25 00:35:25");
INSERT INTO students VALUES("1800567","$2y$10$JySTB0llmzwi.3qDVdHxMOsV6zqeeh9TwbIiaJE9EjlvyHlLmDkgW","Gia Nila","Pantas","Female","Tacloban City","+639845353453","pantas@gmail.com","1","Non-Athlete","Gil Pantas","09123456789","","2","0","2021-06-14 20:11:02");
INSERT INTO students VALUES("1800649","$2y$10$fdMPXJ6zCR08NedU1rD9heDhdvJGh8KXkER0zys3YK/sZW93Md6aq","Jeremiah","Embana","Male","Brgy. 76 Fatima Village Tacloban City, Leyte","+639613057822","jeremiahembana22@gmail.com","5","Athlete","Jeany Embana","09063774018","ID PIC.png","2","1","2021-06-12 20:30:31");



CREATE TABLE `superadmin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `photo` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO superadmin VALUES("1","mis","$2y$10$I5pKP20.JNB7RCWP6lszlugZwbMpElWXBdqM1Rvxgxoe2v2RjmuyG","mis","mis","","2021-06-24 15:04:30");



CREATE TABLE `timein` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(15) NOT NULL,
  `reason` varchar(255) NOT NULL,
  `ttcreated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `student_id` (`student_id`),
  CONSTRAINT `timein_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `students` (`student_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

INSERT INTO timein VALUES("12","1800649","going home","2021-06-29 09:23:34");
INSERT INTO timein VALUES("13","1800649","dffsg","2021-06-29 10:20:08");
INSERT INTO timein VALUES("14","1800649","going home","2021-06-29 23:46:34");



CREATE TABLE `timeout` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(15) NOT NULL,
  `reason` varchar(255) NOT NULL,
  `tcreated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `student_id` (`student_id`),
  CONSTRAINT `timeout_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `students` (`student_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

INSERT INTO timeout VALUES("16","1800649","going home","2021-06-29 09:22:43");
INSERT INTO timeout VALUES("17","1800649","dffsg","2021-06-29 09:50:36");
INSERT INTO timeout VALUES("18","1800649","going home","2021-06-29 10:25:51");
INSERT INTO timeout VALUES("19","1800649","Going home","2021-06-29 23:46:57");



CREATE TABLE `transient` (
  `transient_id` varchar(100) NOT NULL,
  `firstname` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `gender` varchar(100) NOT NULL,
  `address` varchar(255) NOT NULL,
  `contact` varchar(20) NOT NULL,
  `status` int(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`transient_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO transient VALUES("UON1059","Jeremiah","Embana","Male","Brgy. 76 Fatima Village Tacloban City, Leyte","09063774018","1","2021-06-29 09:32:19");



CREATE TABLE `unpaid` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(15) NOT NULL,
  `date_from` date NOT NULL,
  `date_to` date NOT NULL,
  `date_unpaid` timestamp NOT NULL DEFAULT current_timestamp(),
  `status` int(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `student_id` (`student_id`),
  CONSTRAINT `unpaid_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `students` (`student_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=149 DEFAULT CHARSET=latin1;

INSERT INTO unpaid VALUES("147","1800435","2021-06-29","2021-07-29","2021-06-29 09:27:05","0");
INSERT INTO unpaid VALUES("148","1800234","2021-06-29","2021-07-29","2021-06-29 09:27:05","0");

