<!-- <div class="modal fade" id="forgot">
    <div class="modal-dialog">
        <div class="modal-content col-sm-9 col-sm-offset-1 bg-gradient-default">
        <br>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <div class="modal-header bg-gradient-default">
          <h4 class="modal-title text-white text-center"><b>Enter your Email to Reset Password</b></h4>
        </div>
        <div class="modal-body bg-gradient-default">
          <form class="form-horizontal" method="POST" action="includes/forgot.php">
            <div class="form-group">
              <label for="email" class="col-sm-2 control-label text-white">Email</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" id="email" name="email" placeholder="Enter Email" required>
              </div>
            </div>
       
            <div class="modal-footer bg-gradient-default">
            <div class="col text-center">
            <button type="submit" class="btn btn-warning btn-rounded" name="forgot"><i class="fa fa-check"></i> Confirm</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div> -->

<div id="forgot" class="modal fade">
	<div class="modal-dialog modal-login">
		<div class="modal-content">
			<div class="modal-header">			
				<h4 class="modal-title text-white">Enter your Email to Send Reset Password Link</h4>	
        <button type="button" class="close text-white" data-dismiss="modal" aria-hidden="true">&times;</button>
			</div>
      <br>
			<div class="modal-body">
				<form action="includes/forgot.php" method="post">
					<div class="form-group">
						<input type="text" class="form-control" name="email" placeholder="Enter Your Email" required="required" autofocus>		
					</div>
    <br>
					<div class="form-group">
						<button type="submit" class="btn btn-primary btn-lg btn-block login-btn" name="forgot"><i class="fa fa-check"></i><strong>Confirm</strong></button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>