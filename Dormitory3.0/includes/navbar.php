<!-- <li class='dropdown order-1'>
                    <button type='button' id='dropdownMenu1' data-toggle='dropdown' class='btn btn-outline-secondary dropdown-toggle'>Login <span class='caret'></span></button>
                    <ul class='dropdown-menu dropdown-menu-right mt-2'>
                       <li class='px-3 py-2'>
                       <form method='POST' action='login.php'>
                                <div class='form-group'>
                                    <input id='student' name='student' placeholder='Student ID' class='form-control form-control-sm' type='text' required>
                                </div>
                                <div class='form-group'>
                                    <input id='password' name='password' placeholder='Password' class='form-control form-control-sm' type='text' required>
                                </div>
                                <div class='form-group'>
                                    <button type='submit' class='btn btn-warning btn-block' name='login'>Login</button>
                                </div>
                                <div class='form-group text-center'>
                                    <small><a href='#' data-toggle='modal' data-target='#modalPassword'>Forgot password?</a></small>
                                </div>
                            </form>
                        </li>
                    </ul>
                </li> -->
 
<header class="main-header">
  <nav class="navbar navbar-static-top bg-gradient-warning2">
    <div class="container">
      <div class="navbar-header">
        <a href="index.php" class="navbar-brand"><i class="fa fa-building" aria-hidden="true"></i><b>LNU</b>|Dormitory</a>
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
          <i class="fa fa-bars"></i>
        </button>
      </div>

      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
        <ul class="nav navbar-nav">
          <?php
            if(isset($_SESSION['student'])){
              echo "
                <li><a href='equipment.php'><strong>Equipments</strong></a></li>
                <li><a href='transaction.php'><strong>Borrowed&Returned</strong></a></li>
                <li><a href='payment.php'><strong>Payment Records</strong></a></li>
                <li><a href='promissory.php'><strong>Promissory</strong></a></li>
              ";
            } 
          ?>
        </ul>
      </div>
      <!-- /.navbar-collapse -->
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <?php
            if(isset($_SESSION['student'])){
              $photo = (!empty($student['photo'])) ? 'images/'.$student['photo'] : 'images/profile.jpg';
              echo "
              

                <li class='user user-menu'>
                  <a href='#edit' data-toggle='modal' id='student_profile'>
                    <img src='".$photo."' class='user-image' alt='User Image'>
                    <strong><span class='hidden-xs'>".$student['firstname'].' '.$student['lastname']."</span></strong>
                  </a>
                </li>

                <li><a href='logout.php'><i class='fa fa-sign-out'></i> <strong>Log Out</strong></a></li>
              ";
            }
            else{
              echo "
              <li class='float-left'><a href='logbook.php'><strong>Log Book</strong></a></li>
              <li><a href='#forgot' data-toggle='modal'><i class='fa fa-key'></i> <strong>Forgot Password?</strong></a></li>
              <li><a href='#login' data-toggle='modal'><i class='fa fa-sign-in'></i> <strong>Log In</strong></a></li>


                

              ";
            } 
          ?>
        </ul>
      </div>
      <!-- /.navbar-custom-menu -->
    </div>
    <!-- /.container-fluid -->
  </nav>
</header>

<?php include 'includes/login_modal.php'; ?>
<?php include 'includes/forgot_modal.php'; ?>
<?php include 'includes/profile_modal.php'; ?>


<!-- <script>

function getRow(id){
  $.ajax({
    type: 'POST',
    url: 'profile_row.php',
    data: {id:id},
    dataType: 'json',
    success: function(response){
      $('.studid').val(response.studid);
      $('#edit_firstname').val(response.firstname);
      $('#edit_lastname').val(response.lastname);
      $('#edit_gender').val(response.gender);
      $('#edit_address').val(response.address);
      $('#edit_contact').val(response.contact);
      $('#selroom').val(response.room_id);
      $('#selroom').html(response.description);
      $('#selcourse').val(response.course_id);
      $('#selcourse').html(response.code);
    }
  });
}
	</script> -->