<!DOCTYPE html>
<html>
<head>
  	<meta charset="utf-8">
  	<meta http-equiv="X-UA-Compatible" content="IE=edge">
  	<title>LNU Dormitory | Users</title>
	<link rel="icon" href="admin/assets/img/logo.png" type="image/png">
  	<!-- Tell the browser to be responsive to screen width -->
  	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  	<!-- Bootstrap 3.3.7 -->
  	<link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  	<!-- DataTables -->
    <link rel="stylesheet" href="bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  	<!-- Font Awesome -->
  	<link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  	<!-- Theme style -->
  	<link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  	<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  	<link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
	<link href="https://cdn.jsdelivr.net/npm/fullcalendar@5.8.0/main.css" rel="stylesheet" />
  	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  	<!--[if lt IE 9]>
  	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  	<![endif]-->

<link href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round" rel="stylesheet">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


<!-- <script src="http://code.jquery.com/jquery-1.9.1.min.js"></script> -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/css/toastr.css" rel="stylesheet"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/js/toastr.js"></script>

<!-- daterange picker -->
    <link rel="stylesheet" href="bower_components/bootstrap-daterangepicker/daterangepicker.css">
    <!-- Bootstrap time Picker -->
    <link rel="stylesheet" href="plugins/timepicker/bootstrap-timepicker.min.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
	  
  	<style>
body {
		font-family: 'Varela Round', sans-serif;
	}
	.modal-login {		
		color: #636363;
		width: 350px;
		margin: 80px auto 0;
	}
	.modal-login .modal-content {
		background: linear-gradient(87deg, #172b4d 0, #1a174d 100%) !important;
		padding: 20px;
		padding-bottom: 0;
		border-radius: 5px;
		border: none;
	}
	.modal-login .modal-header {
		border-bottom: none;   
        position: relative;
        justify-content: center;
	}
	.modal-login h4 {
		text-align: center;
		font-size: 26px;
		margin: 30px 0 -15px;
	}
	.modal-login .form-control:focus {
		border-color: #70c5c0;
	}
	.modal-login .form-control, .modal-login .btn {
		min-height: 30px;
		border-radius: 3px; 
	}
	.modal-login .close {
        position: absolute;
		top: -5px;
		right: -5px;
	}	
	.modal-login .modal-footer {
		background: linear-gradient(87deg, #1a174d 0, #172b4d 100%) !important;
		border-color: #dee4e7;
		text-align: center;
        justify-content: center;
		margin: 0 -20px -20px;
		border-radius: 5px;
		font-size: 13px;
	}
	.modal-login .modal-footer a {
		color: #999;
	}		
	.modal-login .avatar {
		position: absolute;
		margin: 0 auto;
		left: 0;
		right: 0;
		top: -70px;
		width: 105px;
		height: 105px;
		border-radius: 50%;
		z-index: 9;
		background: linear-gradient(87deg, #fbb140 0, #fb6340 100%) !important;
		padding: 0px;
		box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.1);
	}
	.modal-login .avatar img {
		width: 100%;
	}
    .modal-login .btn {
        color: #fff;
        border-radius: 4px;
		background: linear-gradient(87deg, #fb6340 0, #fbb140 100%) !important;
		text-decoration: none;
		transition: all 0.4s;
        line-height: normal;
        border: none;
		width: 50%;
		margin: 0 auto;
		left: 0;
		right: 0;

		
		
    }
	.modal-login .btn:hover, .modal-login .btn:focus {
		background: linear-gradient(87deg, #fbb140 0, #fb6340 100%) !important;
		outline: none;
	}
	.trigger-btn {
		display: inline-block;
		margin: 100px auto;
	}


.dataTables_filter {
display: none; 
}
.text-white
{
    color: #fff !important;
}
.text-default
{
    color: #172b4d !important;
}
      .bg-gradient-default
{
    background: linear-gradient(87deg, #172b4d 0, #1a174d 100%) !important;
}
.bg-gradient-warning
{
    background: linear-gradient(87deg, #fb6340 0, #fbb140 100%) !important;
}
.bg-gradient-warning2
{
    background: linear-gradient(87deg, #fbb140 0, #fb6340 100%) !important;
}
.bg-orange
{
    background-color: #fb6340 !important;
}
.bg-default
{
    background-color: #172b4d !important;
}
.bg-gradient-dark
{
    background: linear-gradient(87deg, #3f4041 0, #727485 100%) !important;
}
.bg-gradient-info
{
    background: linear-gradient(87deg, #11cdef 0, #1171ef 100%) !important;
}
.bg-gradient-danger
{
    background: linear-gradient(87deg, #f5365c 0, #f56036 100%) !important;
}
.bg-gradient-blue
{
    background: linear-gradient(87deg, #5e72e4 0, #825ee4 100%) !important;
}
.bg-gradient-green
{
    background: linear-gradient(87deg, #2dce89 0, #2dcecc 100%) !important;
}

.bg-gradient-teal
{
    background: linear-gradient(87deg, #11cdef 0, #1171ef 100%) !important;
}
.bg-gradient-pink
{
    background: linear-gradient(87deg, #f3a4b5 0, #f3b4a4 100%) !important;
}
.bg-gradient-gray
{
    background: linear-gradient(87deg, #8898aa 0, #888aaa 100%) !important;
}

.bg-gradient-gray-dark
{
    background: linear-gradient(87deg, #32325d 0, #44325d 100%) !important;
}
.bg-gradient-indigo
{
    background: linear-gradient(87deg, #5603ad 0, #9d03ad 100%) !important;
}
.bg-dark2
{
    background-color: #222d32 !important;
}


button:focus {
    outline: 0;
}

.dropdown-menu1 .form-control1 {
    width: 200px;
}
  	</style>		
</head>