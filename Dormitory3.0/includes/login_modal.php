<!-- Login -->
<!-- <div class="modal fade" id="login">
    <div class="modal-dialog">
        <div class="modal-content col-sm-9 col-sm-offset-1 bg-gradient-default">
        <br>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
            <div class="modal-header bg-gradient-default">
              
              <h4 class="modal-title text-white text-center"><b>Student Login</b></h4>
            </div>
            <div class="modal-body bg-gradient-default">
              <form class="form-horizontal" method="POST" action="login.php">
                <div class="form-group">
                    <label for="student" class="col-sm-3 control-label text-white">Student ID</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control" id="student" name="student" placeholder="Enter Student ID" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="password" class="col-sm-3 control-label text-white">Password</label>

                    <div class="col-sm-9">
                      <input type="password" class="form-control" id="password" name="password" placeholder="Enter Password" required>
                    </div>
                </div>
                
            </div>
            
            <div class="modal-footer bg-gradient-default">
            <div class="col text-center">
            <button type="submit" class="btn btn-success btn-rounded" name="login"><i class="fa fa-sign-in"></i> Login</button>
            </div>
              </form>
            </div>
            
        </div>
    </div>
</div> -->




<div id="login" class="modal fade">
	<div class="modal-dialog modal-login">
		<div class="modal-content">
			<div class="modal-header">
				<div class="avatar">
					<img src="./images/logo.png" alt="Avatar">
				</div>				
				<h4 class="modal-title text-white"><b>Student Login</b></h4>	
                <button type="button" class="close text-white" data-dismiss="modal" aria-hidden="true">&times;</button>
			</div>
			<div class="modal-body">
				<form action="login.php" method="post">
					<div class="form-group">
						<input type="text" class="form-control" name="student" placeholder="Enter Student ID" required="required">		
					</div>
					<div class="form-group">
						<input type="password" class="form-control" name="password" placeholder="Enter Password" required="required">	
					</div>
          <br>
                  
					<div class="form-group">
						<button type="submit" class="btn btn-primary btn-lg btn-block login-btn" name="login"><strong>Login</strong></button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>