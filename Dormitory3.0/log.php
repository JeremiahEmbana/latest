<?php include 'includes/session.php'; ?>
<?php
if (!isset($_SESSION['student']) || trim($_SESSION['student']) == '') {
	header('index.php');
}

$stuid = $_SESSION['student'];
$sql = "SELECT * FROM timeout WHERE student_id = '$stuid' ORDER BY created_at DESC";
$action = '';


?>
<?php include 'includes/header.php'; ?>

<body class="hold-transition skin-blue layout-top-nav">
	<div class="wrapper">

		<?php include 'includes/navbar.php'; ?>

		<div class="content-wrapper bg-gradient-default">
			<div class="container">

				<!-- Main content -->
				<section class="content">
				<<?php
        if(isset($_SESSION['error'])){
        //   echo "
        //     <div class='alert alert-danger alert-dismissible'>
        //       <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
        //       <h4><i class='icon fa fa-warning'></i> Error!</h4>
        //       ".$_SESSION['error']."
        //     </div>
        //   ";
		echo "'<script type='text/javascript'>toastr.error('Error!&nbsp;&nbsp;&nbsp;&nbsp;".$_SESSION['error']."')</script>';";
          unset($_SESSION['error']);
        }
        if(isset($_SESSION['success'])){
        //   echo "
        //     <div class='alert alert-success alert-dismissible'>
        //       <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
        //       <h4><i class='icon fa fa-check'></i> Success!</h4>
        //       ".$_SESSION['success']."
        //     </div>
        //   ";
		echo "'<script type='text/javascript'>toastr.success('Success!&nbsp;&nbsp;&nbsp;&nbsp;".$_SESSION['success']."')</script>';";
          unset($_SESSION['success']);
        }
      ?>
					
							<div class="box">
							
								<div class="box-header with-border">
									<h3 class="box-title">Log Record</h3>
									<div class="pull-right">
										<select class="form-control input-sm" id="transelect">
											<option value="log" <?php echo ($action == '') ? 'selected' : ''; ?>>Logs</option>
										</select>
									</div>
                                    
								</div>
								
								<div class="box-header with-border">
                                <a href="#addnew" data-toggle="modal" class="btn btn-danger btn-sm btn-rounded"><i class="fa fa-sign-out"></i> TimeOut</a>
								<div class="input-group col-sm-3 pull-right">
				                <input type="text" class="form-control input-md" id="searchBox" placeholder="Search...">
				                <span class="input-group-btn">
				                    <button type="button" class="btn btn-primary btn-rounded btn-md"><i class="fa fa-search"></i> </button>
				                </span>
				            </div>
								</div>

								<div class="box-body">
				 				 <div class="table-responsive">
									<table class="table table-bordered table-striped" id="booklist">
										<thead>
											<th class="hidden"></th>
                                            <th>Time-Out Date</th>
                                            <th>Note</th>
											<th>Action</th>
										</thead>
										<tbody>
											<?php
										 $sql = "SELECT *, stud_timeout.id AS timid FROM stud_timeout WHERE student_id = '$stuid'";	
											$query = $conn->query($sql);
											while ($row = $query->fetch_assoc()) {
											
											
												echo "
			        						<tr>
			        							<td class='hidden'></td>
			        							<td>" . date('M d, Y   (h:ia)', strtotime($row['created_at'])) . "</td>
                                                <td>" . $row['reason'] . "</td>
												<td>
                                                <button data-toggle='modal'  class='timein btn btn-success btn-sm btn-rounded' data-id='" . $row['timid'] . "'><i class='fa fa-check'></i> Time In</button>
                                                </td>
			        						</tr>
			        					";
											}
											?>
										</tbody>
									</table>
								</div>
								</div>
							</div>

				</section>

			</div>
		</div>

		<?php include 'includes/footer.php'; ?>
		<?php include 'includes/timeout_modal.php'; ?>
        <?php include 'includes/timein_modal.php'; ?>
	</div>

	<?php include 'includes/scripts.php'; ?>
	<script>
		$('#transelect').on('change', function() {
			var action = $(this).val();
			if (action == 'log') {
				window.location = 'log.php';
			} else {
				window.location = 'log.php?action=' + action;
			}
		});

$(function(){
  $(document).on('click', '.timein', function(e){
    e.preventDefault();
    $('#timein').modal('show');
    var id = $(this).data('id');
    getRow(id);
  });



});

function getRow(id){
  $.ajax({
    type: 'POST',
    url: 'log_row.php',
    data: {id:id},
    dataType: 'json',
    success: function(response){
      $('.timid').val(response.timid);
      $('#edit_note').val(response.reason);

    }
  });
}

</script>
</body>

</html>