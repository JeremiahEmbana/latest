<?php
	include 'includes/session.php';

	if(isset($_POST['add'])){

		$firstname = $_POST['firstname'];
		$lastname = $_POST['lastname'];
		$gender = $_POST['gender'];
		$address = $_POST['address'];
		$contact = $_POST['contact'];

		$room_id = $_POST['room'];
        $date_in = $_POST['date_in'];
        $time_in = $_POST['time_in'];
        $date_out = $_POST['date_out'];
        $time_out = $_POST['time_out'];
		
		$added = 0;
		//creating transientid
		$letters = '';
		$numbers = '';
		foreach (range('A', 'Z') as $char) {
		    $letters .= $char;
		}
		for($i = 0; $i < 10; $i++){
			$numbers .= $i;
		}
		$transient_id = substr(str_shuffle($letters), 0, 3).substr(str_shuffle($numbers), 0, 4);
		//
		$sql = "INSERT INTO transient (transient_id, firstname, lastname, gender, address, contact) VALUES ('$transient_id', '$firstname', '$lastname', '$gender', '$address', '$contact')";
		if($conn->query($sql)){
			$sql = "INSERT INTO checkin (transient_id, room_id, date_in, time_in, date_out, time_out) VALUES ('$transient_id', '$room_id', '$date_in', '$time_in', '$date_out', '$time_out')";
			$conn->query($sql);
			$added++;
			$sql = "UPDATE transient SET status = $added WHERE transient_id = '$transient_id'";
			$conn->query($sql);
			$_SESSION['success'] = 'Transient added successfully';
		}
		else{
			$_SESSION['error'] = $conn->error;
		}

	}
	else{
		$_SESSION['error'] = 'Fill up add form first';
	}

	header('location: transient.php');
?>