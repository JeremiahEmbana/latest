<?php
include 'includes/session.php';

if (isset($_POST['checkin'])) {
	$transient = $_POST['transient_id'];

	$sql = "SELECT * FROM transient WHERE transient_id = '$transient'";
	$query = $conn->query($sql);
	if ($query->num_rows < 1) {
		if (!isset($_SESSION['error'])) {
			$_SESSION['error'] = array();
		}
		$_SESSION['error'][] = 'Transient not found';
	} else {
        $row = $query->fetch_assoc();
		$transient_id = $row['transient_id'];  //
		$added = 0;

		if ($query->num_rows > 0) {

        $room_id = $_POST['room'];
        $date_in = $_POST['date_in'];
        $time_in = $_POST['time_in'];
        $date_out = $_POST['date_out'];
        $time_out = $_POST['time_out'];

        
        // $sql = "SELECT * FROM transient WHERE transient_id = '$transient'";
	    // $query = $conn->query($sql);
		$sql = "INSERT INTO checkin (transient_id, room_id, date_in, time_in, date_out, time_out) VALUES ('$transient_id', '$room_id', '$date_in', '$time_in', '$date_out', '$time_out')";
			if ($conn->query($sql)) {
			$added++;
			$sql = "UPDATE transient SET status = $added WHERE transient_id = '$transient_id'";
			$conn->query($sql);
			
			

					} else {
						if (!isset($_SESSION['error'])) {
							$_SESSION['error'] = array();
						}
						$_SESSION['error'][] = $conn->error;
					}
                }

		if ($added > 0) {
			$equipments = ($added == 1) ? 'Transient' : 'Transients';
			$_SESSION['success'] = $added . ' ' . $equipments . ' Checked In Successfully';
		}
	}
} else {
	$_SESSION['error'] = 'Fill up add form first';
}

header('location: checkin.php');
