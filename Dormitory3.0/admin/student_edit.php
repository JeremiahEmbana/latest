<?php
	include 'includes/session.php';

	if(isset($_POST['edit'])){
		$id = $_POST['id'];
		$student_id = $_POST['student_id'];
		$firstname = $_POST['firstname'];
		$lastname = $_POST['lastname'];
		$privilege = $_POST['privilege'];
		$gender = $_POST['gender'];
		$address = $_POST['address'];
		$contact = $_POST['contact'];
		$email = $_POST['email'];
		$guardian = $_POST['guardian'];
		$guardian_contact = $_POST['guardian_contact'];
		$rooms = $_POST['room'];
		$course = $_POST['course'];

		$sql = "UPDATE students SET student_id = '$student_id', firstname = '$firstname', lastname = '$lastname', privilege = '$privilege', gender = '$gender', address = '$address', contact = '$contact', email = '$email', guardian = '$guardian', guardian_contact = '$guardian_contact', room_id = '$rooms', course_id = '$course' WHERE student_id = '$id'";
		if($conn->query($sql)){
			$_SESSION['success'] = 'Student updated successfully';
		}
		else{
			$_SESSION['error'] = $conn->error;
		}
	}
	else{
		$_SESSION['error'] = 'Fill up edit form first';
	}

	header('location:student.php');

?>