<?php include 'includes/session.php'; ?>
<?php include 'includes/header.php'; ?>

<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

    <?php include 'includes/navbar.php'; ?>
    <?php include 'includes/menubar.php'; ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper bg-gradient-default">
      <!-- Content Header (Page header) -->
      <section class="content-header text-white">
        <h1>
          Unpaid Status
        </h1>
        <ol class="breadcrumb bg-default">
          <li><a href="#" class="text-white"><i class="fa fa-dashboard"></i> Home</a></li>
          <li>Payment Records</li>
          <li class="active text-white">Unpaid Status</li>
        </ol>
      </section>
      <!-- Main content -->
      <section class="content">
      <?php
        if(isset($_SESSION['error'])){
          // echo "
          //   <div class='alert alert-danger alert-dismissible'>
          //     <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
          //     <h4><i class='icon fa fa-warning'></i> Error!</h4>
          //     ".$_SESSION['error']."
          //   </div>
          // ";
          echo "'<script type='text/javascript'>toastr.error('Error!&nbsp;&nbsp;&nbsp;&nbsp;".$_SESSION['error']."')</script>';";
          unset($_SESSION['error']);
        }
        if(isset($_SESSION['success'])){
          // echo "
          //   <div class='alert alert-success alert-dismissible'>
          //     <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
          //     <h4><i class='icon fa fa-check'></i> Success!</h4>
          //     ".$_SESSION['success']."
          //   </div>
          // ";
          echo "'<script type='text/javascript'>toastr.success('Success!&nbsp;&nbsp;&nbsp;&nbsp;".$_SESSION['success']."')</script>';";
          unset($_SESSION['success']);
        }
      ?>
        <div class="row">
          <div class="col-xs-12">
            <div class="box">
              <div class="box-header with-border">
                <a href="#addnew" data-toggle="modal" class="btn btn-success btn-sm btn-rounded"><i class="fa fa-plus"></i> Record Unpaid Student</a>
              </div>

              <div class="box-body">
            <div class="table-responsive">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                    <th class="hidden"></th>
                    <th>Date</th>
                    <th>Student ID</th>
                    <th>Name</th>
                    <th>Privilege</th>
                    <th>Valid From</th>
                    <th>Valid To</th>
                    <th>Status</th>
                    <th>Actions</th>
                  </thead>
                  <tbody>
                    <?php
                    $sql = "SELECT *, unpaid.student_id AS stud, unpaid.status AS pstat FROM unpaid LEFT JOIN students ON students.student_id=unpaid.student_id ORDER BY date_from, date_to DESC";
                    $query = $conn->query($sql);
                    while ($row = $query->fetch_assoc()) {
                      if ($row['pstat']) {
                        $status = '<span class="label label-success">Paid</span>';
                      } else {
                        $status = '<span class="label label-danger">Unpaid</span>';
                      }
                      echo "
                        <tr>
                          <td class='hidden'></td>
                          <td>" . date('M d, Y', strtotime($row['date_unpaid'])) . "</td>
                          <td>" . $row['stud'] . "</td>
                          <td>" . $row['firstname'] . ' ' . $row['lastname'] . "</td>
                          <td>" . $row['privilege'] . "</td>
                          <td>" . date('M d, Y', strtotime($row['date_from'])) . "</td>
                          <td>" . date('M d, Y', strtotime($row['date_to'])) . "</td>
                          <td>" . $status . "</td>
                          <td>
                         
                          <button data-toggle='modal'  class='pay btn btn-warning btn-sm btn-rounded' data-id='" . $row['stud'] . "'><i class='fa fa-check'></i> PAY</button>&nbsp;&nbsp;&nbsp;
                          <button data-toggle='modal'  class='promissory btn btn-primary btn-sm btn-rounded' data-id='" . $row['stud'] . "'><i class='fa fa-edit'></i> Promissory</button>
                          </td>
                        </tr>
                      ";
                    }
                    ?>
                  </tbody>
                </table>
              </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>

    <?php include 'includes/footer.php'; ?>
    <?php include 'includes/unpaid_modal.php'; ?>
    <?php include 'includes/pay_modal.php'; ?>
  </div>
  <?php include 'includes/scripts.php'; ?>
  <script>
$(function() {
      $(document).on('click', '#append', function(e) {
        e.preventDefault();
        $('#append-div').append(
          '<div class="form-group"><label for="" class="col-sm-3 control-label text-white">Student ID</label><div class="col-sm-9"><input type="text" class="form-control" placeholder="Enter Student Number" name="student_id[]"></div></div>'
        );
      });
    });


    $(document).on('click', '.pay', function(e) {
      e.preventDefault();
      var id = $(this).data();
      getRow(id.id);
      $('#pay').modal('show');


    });

    $(document).on('click', '.promissory', function(e) {
      e.preventDefault();
      var id = $(this).data();
      getRow(id.id);
      $('#promissory').modal('show');


    });

    function getRow(id) {
      $.ajax({
        type: 'POST',
        url: 'pay_row.php',
        data: {
          id: id
        },
        dataType: 'json',
        success: function(response) {
          console.log(response);
          $('.studid').val(response.id);
          $('.student_id').val(response.student_id);
          $('.validfrom').val(response.date_from);
          $('.validto').val(response.date_to);
          $('#name').val(response.firstname+' '+response.lastname);
          $('#pname').val(response.firstname+' '+response.lastname);
        }
      });
    }
  </script>
</body>

</html>