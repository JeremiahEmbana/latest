<?php include 'includes/session.php'; ?>
<?php
if (!isset($_SESSION['student']) || trim($_SESSION['student']) == '') {
	header('index.php');
}

$stuid = $_SESSION['student'];
$sql = "SELECT * FROM promissory WHERE student_id = '$stuid' ORDER BY date_promissory DESC";
$action = '';


?>
<?php include 'includes/header.php'; ?>

<body class="hold-transition skin-blue layout-top-nav">
	<div class="wrapper">

		<?php include 'includes/navbar.php'; ?>

		<div class="content-wrapper bg-gradient-default">
			<div class="container">

				<!-- Main content -->
				<section class="content">
				<<?php
        if(isset($_SESSION['error'])){
        //   echo "
        //     <div class='alert alert-danger alert-dismissible'>
        //       <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
        //       <h4><i class='icon fa fa-warning'></i> Error!</h4>
        //       ".$_SESSION['error']."
        //     </div>
        //   ";
		echo "'<script type='text/javascript'>toastr.error('Error!&nbsp;&nbsp;&nbsp;&nbsp;".$_SESSION['error']."')</script>';";
          unset($_SESSION['error']);
        }
        if(isset($_SESSION['success'])){
        //   echo "
        //     <div class='alert alert-success alert-dismissible'>
        //       <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
        //       <h4><i class='icon fa fa-check'></i> Success!</h4>
        //       ".$_SESSION['success']."
        //     </div>
        //   ";
		echo "'<script type='text/javascript'>toastr.success('Success!&nbsp;&nbsp;&nbsp;&nbsp;".$_SESSION['success']."')</script>';";
          unset($_SESSION['success']);
        }
      ?>
					
							<div class="box">
							
								<div class="box-header with-border">
									<h3 class="box-title">Promissory Record</h3>
									<div class="pull-right">
										<select class="form-control input-sm" id="transelect">
											<option value="promissory" <?php echo ($action == '') ? 'selected' : ''; ?>>Promissory</option>
										</select>
									</div>
								</div>
								
								<div class="box-header with-border">
								<div class="input-group col-sm-3 pull-right">
				                <input type="text" class="form-control input-md" id="searchBox" placeholder="Search...">
				                <span class="input-group-btn">
				                    <button type="button" class="btn btn-primary btn-rounded btn-md"><i class="fa fa-search"></i> </button>
				                </span>
				            </div>
								</div>

								<div class="box-body">
				 				 <div class="table-responsive">
									<table class="table table-bordered table-striped" id="booklist">
										<thead>
											<th class="hidden"></th>
                                            <th>Date</th>
											<th>Valid From</th>
											<th>Valid To</th>
                                            <th>Promissory Note</th>
                                            <th>Deadline of Payment</th>
											<th>Status</th>
										</thead>
										<tbody>
											<?php
											
											$query = $conn->query($sql);
											while ($row = $query->fetch_assoc()) {
												if ($row['status']) {
													$status = '<span class="label label-success">Paid</span>';
												  } else {
													$status = '<span class="label label-danger">Unpaid</span>';
												  }
												$date = (isset($_GET['action'])) ? 'date_promissory' : 'date_promissory';
												echo "
			        						<tr>
			        							<td class='hidden'></td>
                                                <td>" . date('M d, Y', strtotime($row[$date])) . "</td>
			        							<td>" . date('M d, Y', strtotime($row['date_from'])) . "</td>
			        							<td>" . date('M d, Y', strtotime($row['date_to'])) . "</td>
                                                <td>" . $row['pnote'] . "</td>
                                                <td>" . date('M d, Y', strtotime($row['deadline'])) . "</td>
												<td>" . $status . "</td>
			        						</tr>
			        					";
											}
											?>
										</tbody>
									</table>
								</div>
								</div>
							</div>

				</section>

			</div>
		</div>

		<?php include 'includes/footer.php'; ?>
		
	</div>

	<?php include 'includes/scripts.php'; ?>
	<script>
		$('#transelect').on('change', function() {
			var action = $(this).val();
			if (action == 'promissory') {
				window.location = 'promissory.php';
			} else {
				window.location = 'promissory.php?action=' + action;
			}
		});


	</script>
</body>

</html>